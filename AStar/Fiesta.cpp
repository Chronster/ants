#include "precomp.h"
#include "Fiesta.h"

Fiesta::Fiesta()
{
}

Fiesta::Fiesta(Canvas &gc, Board* board, Field* start, Field* target)
{
	_sprite = Sprite(gc, "Resources/jeep.png");
	_speed = 12.5f;

	_board = board;

	if(start->GetCost() >= 0)
	{
		_start = start;
	}
	else
	{
		std::vector<Field*> neighbors = start->GetNeighbors();

		for (vector<Field*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if((*it)->GetCost() >= 0)
			{
				_start = (*it);
				break;
			}
		}
	}

	if(target->GetCost() >= 0)
	{
		_target = target;
	}
	else
	{
		std::vector<Field*> neighbors = target->GetNeighbors();

		for (vector<Field*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if((*it)->GetCost() >= 0)
			{
				_target = (*it);
				break;
			}
		}
	}

	_board->SetStart(_start->GetXPos(), _start->GetYPos(), true);
	_board->SetFinish(_target->GetXPos(), _target->GetYPos(), true);

	_position = _start;
	_position->SetState(3);

	_closedList.push_back(_position);

	SetPosition(_position);
}

Fiesta::~Fiesta()
{
}

void Fiesta::Render(Canvas &gc, bool showDebugInfo)
{
	_sprite.draw(gc, _screenPosition.x, _screenPosition.y);

	if(showDebugInfo)
	{
		// render open info
		for (vector<Field*>::iterator it = _openList.begin(); it!=_openList.end(); ++it)
		{
			(*it)->RenderInfo(gc);
		}

		// render closed info
		for (vector<Field*>::iterator it = _closedList.begin(); it!=_closedList.end(); ++it)
		{
			if((*it) != _position && (*it) != _target)
			{
				(*it)->RenderInfo(gc);
			}
		}

		// redner way
		for (vector<Field*>::iterator it = _calculatedWay.begin(); it!=_calculatedWay.end(); ++it)
		{
			if((*it) != _position && (*it) != _target)
			{
				(*it)->RenderInfo(gc);
			}
		}
	}
}

void Fiesta::Update()
{
	_screenPosition.x = _screenPosition.x + (_movementDirection.x * (_speed / _position->GetCost()));
	_screenPosition.y = _screenPosition.y + (_movementDirection.y * (_speed / _position->GetCost()));

	if(_calculatedWay.size() > 0)
	{
		_movementDirection =  Vec2<float>(_calculatedWay[0]->GetScreenXPos() - _screenPosition.x, _calculatedWay[0]->GetScreenYPos() - _screenPosition.y);
		_movementDirection.normalize();

		// check if the target point is reached
		if((int)_screenPosition.x >= (_calculatedWay[0]->GetScreenXPos() - _speed) &&
			(int)_screenPosition.x <= (_calculatedWay[0]->GetScreenXPos() + _speed) &&
			(int)_screenPosition.y >= (_calculatedWay[0]->GetScreenYPos() - _speed) &&
			(int)_screenPosition.y <= (_calculatedWay[0]->GetScreenYPos() + _speed))
		{
			// remove point from list
			_position = _calculatedWay[0];
		}
	}
	else
	{
		_board->SetStart(_start->GetXPos(), _start->GetYPos(), false);
		_board->SetFinish(_target->GetXPos(), _target->GetYPos(), false);

		_target = _start;
		_start = _position;

		_board->SetStart(_start->GetXPos(), _start->GetYPos(), true);
		_board->SetFinish(_target->GetXPos(), _target->GetYPos(), true);
	}

	CalculateWay();
}

bool Fiesta::CalculateWay()
{
	ClearOpenList();
	ClearClosedList();
	ClearCalculatedList();

	return AStar::CalculateWay(_position, _target, &_openList, &_closedList, &_calculatedWay, ROUTE_DEBUG_STATE, false);
}

void Fiesta::SetPosition(Field* position)
{
	vector<Field*> neighbors = _position->GetNeighbors();

	_screenPosition.x = static_cast<float>(_position->GetScreenXPos());
	_screenPosition.y = static_cast<float>(_position->GetScreenYPos());
}

void Fiesta::ClearOpenList()
{
	if(ROUTE_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _openList.begin(); it!=_openList.end(); ++it)
		{
			(*it)->SetState(0);
			(*it)->SetCameFrom(NULL);
			(*it)->SetG(0.0f);
			(*it)->SetH(0.0f);
		}
	}

	_openList.clear();
}

void Fiesta::ClearClosedList()
{
	if(ROUTE_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _closedList.begin(); it!=_closedList.end(); ++it)
		{
			(*it)->SetState(0);
			(*it)->SetCameFrom(NULL);
			(*it)->SetG(0.0f);
			(*it)->SetH(0.0f);
		}
	}

	_closedList.clear();
}

void Fiesta::ClearCalculatedList()
{
	if(ROUTE_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _calculatedWay.begin(); it!=_calculatedWay.end(); ++it)
		{
			(*it)->SetState(0);
			(*it)->SetCameFrom(NULL);
			(*it)->SetG(0.0f);
			(*it)->SetH(0.0f);
		}
	}

	_calculatedWay.clear();
}

Field* Fiesta::GetPosition() const
{
	return _position;
}