#include "precomp.h"
#include "Board.h"
#include <time.h>

Board::Board()
{
	_defaultFieldState = 0;
}

void Board::Init(Canvas &gc)
{
	_sprStart = Sprite(gc, "Resources/start.png");
	_sprFinish = Sprite(gc, "Resources/finish.png");
	_costSprites.push_back(new Sprite(gc, "Resources/white.png"));
}

Board::~Board()
{
}

void Board::AddFieldType(string name, Sprite* sprite, int weight)
{
	FieldType ft(sprite, weight);
	_fieldTypes[name] = ft;
}

FieldType Board::GetFieldTypeByName(string name)
{
	return _fieldTypes[name];
}

void Board::AddFieldState(int statenumber, Sprite* sprite)
{
	_fieldStates[statenumber] = sprite;
}

Sprite* Board::GetFieldStateSpriteByNumber(int statenumber)
{
	return _fieldStates[statenumber];
}

void Board::CreateRandomBoard()
{
	FieldType ft = _fieldTypes["Asphalt"];
	map<string, FieldType>::const_iterator it;
	srand ( static_cast<unsigned int>(time(NULL)) );
	int stop;
	int count;

	for(int y = 0; y < BOARD_HEIGHT; ++y)
	{
		int length = 0;

		if(y % 2 == 0)
		{
			length = BOARD_WIDTH;
		}
		else
		{
			length = BOARD_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			stop =  rand() % _fieldTypes.size();
			count = 0;
			it = _fieldTypes.begin();
			while(it != _fieldTypes.end())
			{
				if(stop == count)
				{
					ft = it->second;
					break;
				}

				++count;
				++it;
			}

			if (ft.GetWeight() <= 0)
			{
				int waterProbability = rand() % 5;
				if (waterProbability != 4) 
				{
					--x;
					continue;
				}
			}

			_fields[x][y] = Field(_fieldStates, ft, _costSprites, _defaultFieldState, x, y);
		}
	}
}

void Board::GenerateNeighborList()
{
	for(int y = 0; y < BOARD_HEIGHT; ++y)
	{
		int length = 0;
		bool isEven;

		if(y % 2 == 0)
		{
			length = BOARD_WIDTH;
			isEven = true;
		}
		else
		{
			length = BOARD_WIDTH - 1;
			isEven = false;
		}

		for(int x = 0; x < length; ++x)
		{
			//seitliche nachbarn
			if(x > 0)
				_fields[x][y].AddNeighbor(&_fields[x-1][y]);

			if(x < length - 1)
				_fields[x][y].AddNeighbor(&_fields[x+1][y]);

			//obere nachbarn
			if(y > 0)
			{
				 //rechts bei geradem, links bei ungeradem y-wert

				if(isEven) //gerade
				{
					if(x > 0) //links oben
					{
						_fields[x][y].AddNeighbor(&_fields[x-1][y-1]);
					}

					if(x < length - 1) //rechts oben
					{
						_fields[x][y].AddNeighbor(&_fields[x][y-1]);
					}
				}
				else
				{
					_fields[x][y].AddNeighbor(&_fields[x][y-1]); //links oben

					if(x < length) //rechts oben
					{
						_fields[x][y].AddNeighbor(&_fields[x+1][y-1]);
					}
				}
			}

			//untere nachbarn
			if(y < BOARD_HEIGHT - 1)
			{
				if(isEven) //gerade
				{
					if(x > 0) //links unten
					{
						_fields[x][y].AddNeighbor(&_fields[x-1][y+1]);
					}

					if(x < length - 1) //rechts unten
					{
						_fields[x][y].AddNeighbor(&_fields[x][y+1]);
					}
				}
				else
				{
					_fields[x][y].AddNeighbor(&_fields[x][y+1]); // links unten

					if(x < length) //rechts unten
					{
						_fields[x][y].AddNeighbor(&_fields[x+1][y+1]);
					}
				}
			}
		}
	}
}

void Board::Render(Canvas &gc, bool showCostColors, bool showDebugInfo)
{
	float pheromoneLevelMax = 0.0f;

	for(int y = 0; y < BOARD_HEIGHT; ++y)
	{
		int length = 0;

		if(y % 2 == 0)
		{
			length = BOARD_WIDTH;
		}
		else
		{
			length = BOARD_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			if (_fields[x][y].GetPheromoneLevel() > pheromoneLevelMax)
			{
				pheromoneLevelMax = _fields[x][y].GetPheromoneLevel();
			}
		}
	}

	for(int y = 0; y < BOARD_HEIGHT; ++y)
	{
		int length = 0;

		if(y % 2 == 0)
		{
			length = BOARD_WIDTH;
		}
		else
		{
			length = BOARD_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			_fields[x][y].Render(gc, showCostColors, pheromoneLevelMax, showDebugInfo);
		}
	}
}

Field& Board::GetField(int x, int y)
{
	return _fields[x][y];
}

void Board::SetDefaultFieldState(int state)
{
	_defaultFieldState = state;
}

void Board::SetStart(int x, int y, bool isStart)
{
	if(isStart)
	{
		_fields[x][y].SetStart(&_sprStart);
	}
	else
	{
		_fields[x][y].SetStart(NULL);
	}
}

void Board::SetFinish(int x, int y, bool isFinish)
{
	if(isFinish)
	{
		_fields[x][y].SetFinish(&_sprFinish);
	}
	else
	{
		_fields[x][y].SetFinish(NULL);
	}
}

void Board::EvaporatePheromones(float p) 
{
	for(int y = 0; y < BOARD_HEIGHT; ++y)
	{
		int length = 0;

		if(y % 2 == 0)
		{
			length = BOARD_WIDTH;
		}
		else
		{
			length = BOARD_WIDTH - 1;
		}

		for(int x = 0; x < length; ++x)
		{
			_fields[x][y].EvaporatePheromones(p);
		}
	}
}

