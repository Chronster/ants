#pragma once

#include "precomp.h"
#include <map>
#include <string>
#include "FieldType.h"
#include "Field.h"

using namespace clan;

#define BOARD_WIDTH 26 //26
#define BOARD_HEIGHT 20	//20

using namespace std;

typedef map<string, FieldType> FieldTypes;
typedef map<int, Sprite*> FieldStates;

class Board
{

public:
	Board();
	~Board();
	void Init(Canvas &gc);
	void AddFieldType(string name, Sprite* sprite, int weight);
	FieldType GetFieldTypeByName(string name);
	void AddFieldState(int statenumber, Sprite* sprite);
	Sprite* GetFieldStateSpriteByNumber(int statenumber);
	void CreateRandomBoard();
	void GenerateNeighborList();
	void Render(Canvas &gc, bool showCostColors, bool showDebugInfo);
	Field& GetField(int x, int y);
	void SetDefaultFieldState(int state);
	void SetStart(int x, int y, bool isStart);
	void SetFinish(int x, int y, bool isFinish);
	void EvaporatePheromones(float p);

private:
	Field _fields[BOARD_WIDTH][BOARD_HEIGHT];

	FieldTypes _fieldTypes;
	FieldStates _fieldStates;
	int _defaultFieldState;

	Sprite _sprStart;
	Sprite _sprFinish;
	std::vector<Sprite*> _costSprites;
};