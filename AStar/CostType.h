#pragma once

#include "precomp.h"

using namespace clan;

class CostType
{
public:
	CostType();
	CostType(Sprite* sprite, int weight);
	~CostType();
	Sprite* GetSprite();
	int GetWeight() const;

private:
	Sprite* _sprite;
	int _weight;

};