#include "precomp.h"
#include "Ant.h"

Ant::Ant()
{
}

Ant::Ant(Canvas &gc, Board* board, Field* start, float* speed)
{
	_sprite = Sprite(gc, "Resources/ant.png");
	_speed = speed;
	_a = 2.0f;
	_b = 0.4f;
	_isExploring = true;
	_deltaTau = 0.0f;

	_board = board;
	srand(time(NULL));

	if(start->GetCost() >= 0)
	{
		_start = start;
	}
	else
	{
		std::vector<Field*> neighbors = start->GetNeighbors();

		for (vector<Field*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if((*it)->GetCost() >= 0)
			{
				_start = (*it);
				break;
			}
		}
	}

	_board->SetStart(_start->GetXPos(), _start->GetYPos(), true);

	_position = _start;
	_position->SetState(3);

	_closedList.push_back(_position);

	SetPosition(_position);

	_target = NULL;
}

Ant::~Ant()
{
}

void Ant::Render(Canvas &gc)
{
	_sprite.draw(gc, _screenPosition.x, _screenPosition.y);
}

void Ant::Update()
{
	_screenPosition.x = _screenPosition.x + (_movementDirection.x * (*_speed / _position->GetCost()));
	_screenPosition.y = _screenPosition.y + (_movementDirection.y * (*_speed / _position->GetCost()));

	if(_target)
	{
		_movementDirection =  Vec2<float>(_target->GetScreenXPos() - _screenPosition.x, _target->GetScreenYPos() - _screenPosition.y);
		_movementDirection.normalize();

		// check if the target point is reached
		if((int)_screenPosition.x >= (_target->GetScreenXPos() - (*_speed)) &&
			(int)_screenPosition.x <= (_target->GetScreenXPos() + (*_speed)) &&
			(int)_screenPosition.y >= (_target->GetScreenYPos() - (*_speed)) &&
			(int)_screenPosition.y <= (_target->GetScreenYPos() + (*_speed)))
		{
			_position = _target;
			if(_target->HasNutrition() && _isExploring == true)
			{
				CalculateDeltaTau();
				MarkField();
				_closedList.pop_back();
				_target = _closedList.back();
				_closedList.pop_back();
				_isExploring = false;
			
			} else if(!_target->HasNutrition() && _isExploring == true)
			{
				_target = NULL;
			
			} else if(!_target->HasNutrition() && _isExploring == false)
			{
				if (_position != _start) {
					MarkField();
					_target = _closedList.back();
					_closedList.pop_back();
				
				} else {
					_target = NULL;
					_isExploring = true;
					ClearClosedList();
					ClearTrappedList();
					_closedList.push_back(_start);
				}
			}
		}
	}
	else
	{
		CalculateMove();
	}
}

void Ant::CalculateMove()
{
	if (_isExploring)
	{
		ClearOpenList();
		//ClearClosedList();
		double rouletteBall = rand() / double(RAND_MAX + 1);
		vector<Field*> neighbourList = _position->GetNeighbors();
		double sigma = 0.0;
		double p = 0.0;
		double lastP = 0.0;
		bool isVisited;

		for (vector<Field*>::iterator it = neighbourList.begin(); it!=neighbourList.end(); ++it) 
		{
			//ONLY FIELDS WHICH HAVE NOT BEEN VISITED AND WHICH ARE != WATER
			isVisited = false;
			if ((*it)->GetType().GetWeight() > 0)
			{
				for (vector<Field*>::iterator visited = _closedList.begin(); visited!=_closedList.end(); ++visited) 
				{
					if (*it==*visited) 
					{
						isVisited = true;
						break;
					}
				}

				if (isVisited == false)
				{
					for (vector<Field*>::iterator visited = _trappedList.begin(); visited!=_trappedList.end(); ++visited) 
					{
						if (*it==*visited) 
						{
							isVisited = true;
							break;
						}
					}
				}

				if (isVisited == false)
				{
					float pheromoneLevel = (*it)->GetPheromoneLevel();
					if (pheromoneLevel <= 0.0f)
					{
						pheromoneLevel = 1.0f;
					}
					sigma += pow(pheromoneLevel, _a) * pow((*it)->GetCost(), _b);
					_openList.push_back(*it);
				}
			}
		}

		if (_openList.size() > 0) 
		{
			for (vector<Field*>::iterator it = _openList.begin(); it!=_openList.end(); ++it) 
			{
				float pheromoneLevel = (*it)->GetPheromoneLevel();
				if (pheromoneLevel <= 0.0f)
				{
					pheromoneLevel = 1.0f;
				}
				p += (pow(pheromoneLevel, _a) * pow((*it)->GetCost(), _b))/sigma;
				if (rouletteBall >= lastP && rouletteBall <= p)
				{
					_target = *it;
					_closedList.push_back(*it);
					break;
				}
				lastP = p;
			}
	
		} else {
			//There's no way to go. Ant is trapped and has to move back a field;
			if (_closedList.size() > 0)
			{
				_target = _closedList.back();
				_trappedList.push_back(_target);
				_closedList.pop_back();
			
			} else
			{
				_position = _start;
				SetPosition(_position);
				ClearClosedList();
				_closedList.push_back(_start);
			}
		}
	
	} else
	{

	}
}

void Ant::SetPosition(Field* position)
{
	//vector<Field*> neighbors = _position->GetNeighbors();

	_screenPosition.x = static_cast<float>(_position->GetScreenXPos());
	_screenPosition.y = static_cast<float>(_position->GetScreenYPos());
}

void Ant::ClearOpenList()
{
	/*if(ROUTE_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _openList.begin(); it!=_openList.end(); ++it)
		{
			(*it)->SetState(0);
			(*it)->SetCameFrom(NULL);
			(*it)->SetG(0.0f);
			(*it)->SetH(0.0f);
		}
	}*/

	_openList.clear();
}

void Ant::ClearClosedList()
{
	_closedList.clear();
}

void Ant::ClearTrappedList()
{
	_trappedList.clear();
}

Field* Ant::GetPosition() const
{
	return _position;
}

void Ant::CalculateDeltaTau()
{
	float optimalPathWeigth = 0.0f;
	float chosenPathWeigth = 0.0f;
	vector<Field*> openList;
	vector<Field*> closedList;
	vector<Field*> calculatedWay;
	_deltaTau = 0.0f;

	AStar::CalculateWay(_position, _start, &openList, &closedList, &calculatedWay, false, true);
	for(vector<Field*>::iterator it = calculatedWay.begin(); it!=calculatedWay.end(); ++it)
	{
		optimalPathWeigth += (*it)->GetCost();
	}

	for(vector<Field*>::iterator it = _closedList.begin(); it!=_closedList.end(); ++it)
	{
		chosenPathWeigth += (*it)->GetCost();
	}

	_deltaTau += optimalPathWeigth/chosenPathWeigth;
}

void Ant::MarkField()
{
	_position->AddPheromones(_deltaTau);
}