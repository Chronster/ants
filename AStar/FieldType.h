#pragma once

#include "precomp.h"

using namespace clan;

class FieldType
{
public:
	FieldType();
	FieldType(Sprite* sprite, int weight);
	~FieldType();
	Sprite* GetSprite();
	int GetWeight() const;
	void SetWeight(int weight);

private:
	Sprite* _sprite;
	int _weight;

};