#include "precomp.h"
#include "FieldType.h"

using namespace clan;

FieldType::FieldType()
{
}

FieldType::FieldType(Sprite* sprite, int weight): _sprite(sprite), _weight(weight)
{
}

FieldType::~FieldType()
{
}

Sprite* FieldType::GetSprite()
{
	return _sprite;
}

int FieldType::GetWeight() const
{
	return _weight;
}

void FieldType::SetWeight(int weight)
{
	_weight = weight;
}