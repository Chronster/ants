#pragma once

#include "precomp.h"
#include <assert.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <time.h>
#include <math.h>
#include "Board.h"
#include "Field.h"
#include "AStar.h"

using namespace clan;

class Ant
{

public:
	Ant();
	Ant(Canvas &gc, Board* board, Field* start, float* speed);
	~Ant();
	void Render(Canvas &gc);
	void Update();
	void CalculateMove();
	Field* GetPosition() const;

private:
	Board* _board;
	Field* _start;
	Field* _position;
	Field* _target;

	float _a;
	float _b;

	Sprite _sprite;

	vector<Field*> _openList;
	vector<Field*> _closedList;
	vector<Field*> _trappedList;

	bool _isExploring;

	Vec2<float> _screenPosition;
	Vec2<float> _movementDirection;

	float* _speed;
	float _deltaTau;

	void SetPosition(Field* position);

	void ClearOpenList();
	void ClearClosedList();
	void ClearTrappedList();
	void CalculateDeltaTau();
	void MarkField();
};