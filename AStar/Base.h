#pragma once
#include "Board.h"
//#include "Opponent.h"
#include "Ant.h"

using namespace clan;

class Base
{

public:
	Base();
	int start(const std::vector<string> &args);

private:
	void on_input_up(const InputEvent &key);
	void on_mouse_up(const InputEvent &mouse);
	void on_mouse_move(const InputEvent &mouse);
	void on_window_close();

private:
	bool _quit;
	Board _board;
	float _lastUpdate;
	float _lastEvaporization;
	vector<Ant*> _ants;
	bool _showCostColors;
	bool _showDebugInfo;
	bool _showAnts;
	Field* _start;
	Field* _finish;
	Field* _lastField;
	CollisionOutline _outlineTemplate;
	Sprite _sprStart;
	Sprite _sprFinish;
	Sprite _sprFire;
	Canvas* _gc;
	float _speed;
	float _p;
	//bool _showDebugInfo;
};
