#include "precomp.h"
#include "AStar.h"


bool compare(Field* f1, Field* f2)
{
	return *f1 < *f2;
}

bool AStar::CalculateWay(Field* start, Field* target, vector<Field*>* openList, vector<Field*>* closedList, vector<Field*>* calculatedWay, bool setState, bool ignoreExtraCost)
{
	target->SetG(0.0f);
	if(setState)
	{
		target->SetState(2); //known
	}
	openList->push_back(target);

	Field* current;

	while(openList->size() != 0)
	{
		current = openList->back();
		openList->pop_back();
		if(setState)
		{
			current->SetState(3); //visited
		}
		closedList->push_back(current);

		if(current == start)
		{
			// find way reverse
			calculatedWay->clear();

			if(setState)
			{
				current->SetState(4); //1 w�re frame, 4 = way
			}

			do 
			{
				if(current != target)
				{
					current = current->GetCameFrom();
				}

				if(start != target)
				{
					calculatedWay->push_back(current);
				}

				if(setState)
				{
					current->SetState(4); //1 w�re frame, 4 = way
				}

			} while ( current!= target);

			return true;
		}

		bool inClosedList = false;
		bool inOpenList = false;
		for (auto neighbour : current->GetNeighbors())
		{
			inOpenList = false;
			inClosedList = false;
			if (neighbour->GetCost() < 0 )
			{
				continue;
			}

			inClosedList = false;
			for (auto finalized : *closedList)
			{
				if (finalized == neighbour)
				{
					inClosedList = true;
					break;
				}
			}

			if (inClosedList)
			{
				continue;
			}

			float cost;

			if(ignoreExtraCost)
			{
				cost = current->GetG() + neighbour->GetCost();			
			}
			else
			{
				cost = current->GetG() + neighbour->GetCost()+neighbour->GetExtraCost();
			}

			// heuristic
			int dx = abs(neighbour->GetXPos() - start->GetXPos());
			int dy = abs(neighbour->GetYPos() - start->GetYPos());

			if(neighbour->GetYPos() != start->GetYPos())
			{
				if(neighbour->GetYPos() % 2 == start->GetYPos() % 2)
				{
					--dy;
				}
			}

			float heuristic = static_cast<float>(dx + dy);

			// g+h
			float f = cost + heuristic;

			for (auto todo : *openList)
			{
				if (todo == neighbour)
				{
					if (f < todo->GetF())
					{
						todo->SetG(cost);
						todo->SetH(heuristic);
						todo->SetCameFrom(current);
					} 
					inOpenList = true;
					break;
				}
			}
			if (inOpenList)
			{
				continue;
			}

			if(setState)
			{
				neighbour->SetState(2); //known
			}

			neighbour->SetG(cost);
			neighbour->SetH(heuristic);
			neighbour->SetCameFrom(current);

			openList->push_back(neighbour);
		}

		// order the list of fields by their F values
		sort(openList->begin(), openList->end(), compare);
	}
	return false;
}
