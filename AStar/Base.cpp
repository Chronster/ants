#include "precomp.h"
#include "Base.h"
//#include "Fiesta.h"
#include "FieldType.h"

Base::Base()
{
	_showCostColors = true;
	_showDebugInfo = true;
	_showAnts = true;
	_start = NULL;
	_finish = NULL;
	_lastField = NULL;
	_outlineTemplate = CollisionOutline("Resources/grass.png");
	_outlineTemplate.save("outline.out");
	_speed = 2.35f;
	_p = 0.001;
}

// The start of the Application
int Base::start(const std::vector<string> &args)
{
	_quit = false;

	// Set the window
	DisplayWindowDescription desc;
	desc.set_title("Ant Colony Optimization");
	desc.set_size(Size(1664, 980), true);
	desc.set_allow_resize(true);

	DisplayWindow window(desc);

	// Connect the Window close event
	Slot slot_quit = window.sig_window_close().connect(this, &Base::on_window_close);

	// Connect a keyboard handler to on_key_up()
	Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &Base::on_input_up);

	clan::InputDevice mouse = window.get_ic().get_mouse();
	clan::Slot slot = mouse.sig_key_up().connect(this, &Base::on_mouse_up);
	clan::Slot slot_mouse_move = mouse.sig_pointer_move().connect(this, &Base::on_mouse_move);

	// Get the graphic context
	Canvas gc(window);
	_gc = &gc;

	// Load a sprite from a png-file
	_sprStart = Sprite(gc, "Resources/start.png");
	_sprFinish = Sprite(gc, "Resources/finish.png");
	_sprFire = Sprite(gc, "Resources/fire.png");

	//field type sprites
	Sprite spr_asphalt(gc, "Resources/asphalt.png");
	Sprite spr_grass(gc, "Resources/grass.png");
	Sprite spr_sludge(gc, "Resources/sludge.png");
	Sprite spr_water(gc, "Resources/water.png");

	//field state sprites
	Sprite spr_frame(gc, "Resources/frame.png");
	Sprite spr_known(gc, "Resources/known.png");
	Sprite spr_visited(gc, "Resources/visited.png");
	Sprite spr_way(gc, "Resources/way.png");

	//CL_Sprite spr_swat(gc, "Resources/fiesta.png");
	_board.Init(gc);

	_board.AddFieldType("Asphalt", &spr_asphalt, 1);
	_board.AddFieldType("Grass", &spr_grass, 3);
	_board.AddFieldType("Sludge", &spr_sludge, 5);
	_board.AddFieldType("Water", &spr_water, -1);

	_board.AddFieldState(1, &spr_frame);
	_board.AddFieldState(2, &spr_known);
	_board.AddFieldState(3, &spr_visited);
	_board.AddFieldState(4, &spr_way);

	//Frames testen
	_board.SetDefaultFieldState(0);

	_board.CreateRandomBoard();
	_board.GenerateNeighborList();
	
	//Ant ant1 = Ant(gc, &_board, &_board.GetField(24,18));

	//_ants.push_back(&ant1);

	unsigned int last_time = System::get_time();
	_lastUpdate = 0.0f;
	_lastEvaporization = 0.0f;

	// Run until someone presses escape
	while (!_quit)
	{
		unsigned int current_time = System::get_time();
		float time_delta_ms = static_cast<float> (current_time - last_time);
		_lastUpdate += static_cast<int>(time_delta_ms);
		_lastEvaporization += static_cast<int>(time_delta_ms);

		// Clear the display in a dark blue nuance
		// The four arguments are red, green, blue and alpha
		gc.clear(Colorf(0.0f,0.0f,0.2f));

		if(_lastEvaporization > 1000.0f)
		{
			_lastEvaporization = 0.0f;

			if(_speed * _p < 0.5f)
			{
				_board.EvaporatePheromones(_speed * _p);
			
			} else
			{
				_board.EvaporatePheromones(0.5f);
			}
		}

		if(_lastUpdate > 25.0f)
		{
			_lastUpdate = 0.0f;

			if(_finish)
			{
				for (vector<Ant*>::iterator it = _ants.begin(); it!=_ants.end(); ++it)
				{
					(*it)->Update();
				}

				//_fiesta.Update();
			}
		}

		_board.Render(gc, _showCostColors, _showDebugInfo);

		if(_finish && _showAnts)
		{
			//_fiesta.Render(gc, _showDebugInfo);
			for (vector<Ant*>::iterator it = _ants.begin(); it!=_ants.end(); ++it)
			{
				(*it)->Render(gc);
			}
		}

		// Flip the display, showing on the screen what we have drawed
		// since last call to flip()
		window.flip(1);

		// This call processes user input and other events
		KeepAlive::process(0);

		last_time = current_time;
	}

	return 0;
}

// A key was pressed
void Base::on_input_up(const InputEvent &key)
{
	if(key.id == keycode_escape)
	{
		_quit = true;
	
	}

	if(key.id == keycode_add)
	{
		_speed *= 2.0f;
	}

	if(key.id == keycode_subtract)
	{
		if (_speed > 4.0f)
		{
			_speed *= 0.5f;
		}
	}

	if(key.id == keycode_a)
	{
		_showAnts = !_showAnts;
	}
	
	if(key.id == keycode_c)
	{
		_showCostColors = !_showCostColors;
	}

	if(key.id == keycode_d)
	{
		_showDebugInfo = !_showDebugInfo;
	}
}

// If mouse is moved
void Base::on_mouse_move(const InputEvent &mouse)
{
	if(!_finish) 
	{
		Pointf mousePos(mouse.mouse_pos.x, mouse.mouse_pos.y);
		for(int x=0; x<BOARD_WIDTH; x++) 
		{
			for(int y=0; y<BOARD_HEIGHT; y++)
			{
				CollisionOutline outline = _board.GetField(x, y).GetOutline();
				if(outline.point_inside(mousePos) && &_board.GetField(x,y)!=_lastField) {
					if(!_start)
					{
						_board.GetField(x, y).SetStart(&_sprStart);
						if(_lastField)
						{
							_lastField->ClearStart();
						}
						_lastField = &_board.GetField(x, y);

					} else 
					{
						_board.GetField(x, y).SetFinish(&_sprFinish);
						if(_lastField)
						{
							_lastField->ClearFinish();
						}
						_lastField = &_board.GetField(x, y);
					}

				}
			}
		}
	}
}

// if user clicked
void Base::on_mouse_up(const InputEvent &mouse)
{
	if(!_finish && _lastField) 
	{
		if (!_start && _lastField->GetCost()>=0) 
		{
			_start = _lastField;
		
		} else if (_start && !_finish && _lastField->GetCost()>=0 && _start != _lastField)
		{

			//_fiesta = Fiesta(*_gc, &_board, _start, _lastField);

			//_fiesta.CalculateWay();
			for (int i=0; i<50; i++)
			{
				_ants.push_back(new Ant(*_gc, &_board, _start, &_speed));
			}

			for (vector<Ant*>::iterator it = _ants.begin(); it!=_ants.end(); ++it)
			{
				(*it)->CalculateMove();
			}

			_finish = _lastField;
		}
	
	} else
	{
		Pointf mousePos(mouse.mouse_pos.x, mouse.mouse_pos.y);
		for(int x=0; x<BOARD_WIDTH; x++) 
		{
			for(int y=0; y<BOARD_HEIGHT; y++)
			{
				CollisionOutline outline = _board.GetField(x, y).GetOutline();
				if(outline.point_inside(mousePos) && &_board.GetField(x,y)!=_lastField) 
				{
					_board.GetField(x, y).SetFire(&_sprFire);
				}
			}
		}
	}
}

// The window was closed
void Base::on_window_close()
{
	_quit = true;
}