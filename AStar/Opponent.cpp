#include "precomp.h"
#include "Opponent.h"

Opponent::Opponent(Canvas &gc, Board* board, Field* start, Field* target, int cost) : _board(board)
{
	_sprite = Sprite(gc, "Resources/tank.png");

	if(start->GetCost() >= 0)
	{
		_start = start;
	}
	else
	{
		std::vector<Field*> neighbors = start->GetNeighbors();

		for (vector<Field*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if((*it)->GetCost() >= 0)
			{
				_start = (*it);
				break;
			}
		}
	}

	if(target->GetCost() >= 0)
	{
		_target = target;
	}
	else
	{
		std::vector<Field*> neighbors = target->GetNeighbors();

		for (vector<Field*>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
		{
			if((*it)->GetCost() >= 0)
			{
				_target = (*it);
				break;
			}
		}
	}

	_position = _start;

	_cost = cost;
	_neighbourCost = cost*0.75;

	_screenPosition.x = static_cast<float>(_position->GetScreenXPos());
	_screenPosition.y = static_cast<float>(_position->GetScreenYPos());

	// add extra cost to current field
	_position->SetExtraCost(_position->GetExtraCost() + _cost);

	// add extra cost to neighbors
	vector<Field*> neighbors = _position->GetNeighbors();
	for (vector<Field*>::iterator it = neighbors.begin(); it!=neighbors.end(); ++it)
	{
		if ((*it)->GetCost() >= 0)
			(*it)->SetExtraCost((*it)->GetExtraCost() + _neighbourCost);
	}

	_speed = 5.0f;
}

Opponent::~Opponent()
{
}

void Opponent::Render(Canvas &gc)
{
	_sprite.draw(gc, _screenPosition.x, _screenPosition.y);
}

void Opponent::Update()
{
	_screenPosition.x = _screenPosition.x + (_movementDirection.x * (_speed / _position->GetCost()));
	_screenPosition.y = _screenPosition.y + (_movementDirection.y * (_speed / _position->GetCost()));

	if(_calculatedWay.size() > 0)
	{
		_movementDirection = Vec2<float>(_calculatedWay[0]->GetScreenXPos() - _screenPosition.x, _calculatedWay[0]->GetScreenYPos() - _screenPosition.y);
		_movementDirection.normalize();

		if((int)_screenPosition.x >= (_calculatedWay[0]->GetScreenXPos() - _speed) &&
			(int)_screenPosition.x <= (_calculatedWay[0]->GetScreenXPos() + _speed) &&
			(int)_screenPosition.y >= (_calculatedWay[0]->GetScreenYPos() - _speed) &&
			(int)_screenPosition.y <= (_calculatedWay[0]->GetScreenYPos() + _speed))
		{
			// remove point from list
			SetPosition(_calculatedWay[0]);
		}
	}
	else
	{
		_target = _start;
		_start = _position;
	}

	CalculateWay();
}

bool Opponent::CalculateWay()
{
	ClearOpenList();
	ClearClosedList();
	ClearCalculatedList();

	return AStar::CalculateWay(_position, _target, &_openList, &_closedList, &_calculatedWay, OPPONENT_DEBUG_STATE, true);
}

void Opponent::SetPosition(Field* position)
{
	vector<Field*> neighbors;

	// remove extra cost to current field
	_position->SetExtraCost(0);

	// add extra cost to neighbors
	neighbors = _position->GetNeighbors();
	for (vector<Field*>::iterator it = neighbors.begin(); it!=neighbors.end(); ++it)
	{
		(*it)->SetExtraCost(0);
	}

	_position = position;

	// add extra cost to current field
	_position->SetExtraCost(_start->GetExtraCost() + _cost);

	// add extra cost to neighbors
	neighbors = _position->GetNeighbors();
	for (vector<Field*>::iterator it = neighbors.begin(); it!=neighbors.end(); ++it)
	{
		if ((*it)->GetCost() >= 0)
			(*it)->SetExtraCost((*it)->GetExtraCost() + _neighbourCost);
	}

	_screenPosition.x = static_cast<float>(_position->GetScreenXPos());
	_screenPosition.y = static_cast<float>(_position->GetScreenYPos());

	_movementDirection = Vec2<float>(_target->GetScreenXPos() - _screenPosition.x, _target->GetScreenYPos() - _screenPosition.y);
	_movementDirection.normalize();
}

void Opponent::ClearOpenList()
{
	if(OPPONENT_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _openList.begin(); it!=_openList.end(); ++it)
		{
			(*it)->SetState(0);
		}
	}

	_openList.clear();
}

void Opponent::ClearClosedList()
{
	if(OPPONENT_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _closedList.begin(); it!=_closedList.end(); ++it)
		{
			(*it)->SetState(0);
		}
	}

	_closedList.clear();
}

void Opponent::ClearCalculatedList()
{
	if(OPPONENT_DEBUG_STATE)
	{
		for (vector<Field*>::iterator it = _calculatedWay.begin(); it!=_calculatedWay.end(); ++it)
		{
			(*it)->SetState(0);
		}
	}

	_calculatedWay.clear();
}