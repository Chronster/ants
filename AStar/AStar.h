#pragma once

#include <assert.h>
#include <vector>
#include <algorithm>

#include "precomp.h"
#include "Board.h"
#include "Field.h"

class AStar
{

public:
	static bool CalculateWay(Field* start, Field* target, vector<Field*>* openList, vector<Field*>* closedList, vector<Field*>* calculatedWay, bool setState, bool ignoreExtraCost);
private:
	static void CheckWaypoint(Field* field, Field* target, vector<Field*>* openList, vector<Field*>* closedList, bool setState, bool ignoreExtraCost);
};