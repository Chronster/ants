#pragma once

#include "precomp.h"
#include "FieldType.h"
#include <map>
#include <vector>

#define FIELD_WIDTH 64
#define FIELD_HEIGHT 48

class Field
{
public:

	Field();
	Field(std::map<int, Sprite*>& states, FieldType type, std::vector<Sprite*>& costSprites, int state, int x, int y);
	~Field();
	void Render(Canvas &gc, bool showCostColors, float pheromoneLevelMax, bool showDebugInfo);
	void RenderInfo(Canvas &gc);
	void AddNeighbor(Field* neighbor);
	int GetNeighborCount() const;
	std::vector<Field*> Field::GetNeighbors() const;
	void SetState(int state);
	int GetState() const;
	int GetScreenXPos(void);
	int GetScreenYPos(void);
	int GetXPos(void);
	int GetYPos(void);
	FieldType& GetType();
	bool operator<(const Field& rhs) const;
	void SortNeighbors();
	void SetG(float g);
	void SetH(float h);
	float GetG(void);
	float GetH(void);
	float GetF(void) const;
	int GetCost(void) const;
	int GetExtraCost(void) const;
	void SetExtraCost(int cost);
	float GetPheromoneLevel(void) const;
	void AddPheromones(float deltaTau);
	void EvaporatePheromones(float p);
	void SetCameFrom(Field* field);
	Field* GetCameFrom(void) const;
	void SetStart(Sprite* start);
	void SetFinish(Sprite* finish);
	void SetFire(Sprite* fire);
	void ClearStart();
	void ClearFinish();
	CollisionOutline GetOutline();
	bool HasNutrition();

	static int GetFieldHeight();
	static int GetFieldWidth();

private:
	std::map<int, Sprite*> _states;
	FieldType _type;
	std::vector<Sprite*> _costSprites;
	int _state; //0: unbekannt, 1: debug - frame anzeige, 2: bekannt, 3: besucht
	int _x;
	int _y;
	float _g;
	float _h;
	
	int _xPos;
	int _yPos;
	std::vector<Field*> _neighbors;
	static Font* _font;

	Field* _cameFrom;
	int _extraCost;
	float _pheromoneLevel;

	Sprite* _sprStart;
	Sprite* _sprFinish;
	Sprite* _sprCost;
	Sprite* _sprFire;

	CollisionOutline _outline;
};