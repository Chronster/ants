#include "precomp.h"
#include "Field.h"
#include <algorithm>

using namespace clan;

Font* Field::_font = 0;

Field::Field()
{
	_sprStart = NULL;
	_sprFinish = NULL;
	_sprFire = NULL;
	_pheromoneLevel = 0;
}

Field::Field(std::map<int, Sprite*>& states, FieldType type, std::vector<Sprite*>& costSprites, int state, int x, int y) : _states(states), _type(type), _costSprites(costSprites), _state(state), _x(x), _y(y), _g(0.0f), _h(0.0f)
{
	_yPos = y * FIELD_HEIGHT;
	_xPos = x * FIELD_WIDTH;
	_extraCost = 0;
	_pheromoneLevel = 0;

	if(y % 2 != 0)
		_xPos += FIELD_WIDTH/2;

	_sprStart = NULL;
	_sprFinish = NULL;
	_sprFire = NULL;
	_outline = CollisionOutline("outline.out");
	_outline.set_translation(static_cast<float>(_xPos), static_cast<float>(_yPos));
}

Field::~Field()
{
}

void Field::Render(Canvas &gc, bool showCostColors, float pheromoneLevelMax, bool showDebugInfo)
{
	float pheromoneLevel = GetPheromoneLevel()/pheromoneLevelMax;
	clan::Colorf c(pheromoneLevel, pheromoneLevel, pheromoneLevel, 1.0f);

	/*if(_state > 0)
		_states[_state]->draw(gc, _xPos, _yPos);*/

	if (showCostColors) {
		_costSprites[0]->set_color(c);
		_costSprites[0]->draw(gc, _xPos, _yPos);
		/*int costsCumulated = this->GetCost()+this->GetExtraCost();
		if(costsCumulated >= 0 && costsCumulated <3)
			_costSprites[0]->draw(gc, _xPos, _yPos);
		if(costsCumulated >= 3 && costsCumulated <10) 
			_costSprites[1]->draw(gc, _xPos, _yPos);
		if(costsCumulated >= 10 && costsCumulated <50) 
			_costSprites[2]->draw(gc, _xPos, _yPos);
		if(costsCumulated >= 50 && costsCumulated <77) 
			_costSprites[3]->draw(gc, _xPos, _yPos);
		if(costsCumulated >= 77 && costsCumulated <83) 
			_costSprites[4]->draw(gc, _xPos, _yPos);
		if(costsCumulated >= 83) 
			_costSprites[5]->draw(gc, _xPos, _yPos);*/
	
	} else 
	{
		_type.GetSprite()->draw(gc, _xPos, _yPos);
	}

	if(_sprStart != NULL)
		_sprStart->draw(gc, _xPos, _yPos);

	if(_sprFinish != NULL)
		_sprFinish->draw(gc, _xPos, _yPos);

	if(_sprFire != NULL)
		_sprFire->draw(gc, _xPos, _yPos);

	if(showDebugInfo) 
	{
		RenderInfo(gc);
	}
}

void Field::RenderInfo(Canvas &gc)
{
	if(Field::_font == 0)
	{
		Field::_font = new Font(gc, "Arial", 14);
	}

	//DEBUG FONTS
	// cost * heuristic - links oben
	//Field::_font->draw_text(gc, _xPos + 5, _yPos + FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height(), StringHelp::float_to_local8(GetF(), 0), Colorf::yellow);
	Field::_font->draw_text(gc, _xPos + 5, _yPos + FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height(), StringHelp::float_to_local8(GetPheromoneLevel(), 3), Colorf::yellow);

	// cost - rechts oben
	//Field::_font->draw_text(gc, _xPos + FIELD_WIDTH - (5 + _font->get_text_size(gc, StringHelp::float_to_local8(_g, 0)).width), _yPos + FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height(),
	//	StringHelp::float_to_local8(_g, 0), Colorf::white);

	// heuristic - rechts unten
	//Field::_font->draw_text(gc, _xPos + FIELD_WIDTH - (5 + _font->get_text_size(gc, StringHelp::float_to_local8(_h, 0)).width),
	//	_yPos + 3 * FIELD_HEIGHT / 4 + (int)_font->get_font_metrics().get_height() - 5, StringHelp::float_to_local8(_h, 0), Colorf::white);
}

void Field::AddNeighbor(Field* neighbor)
{
	_neighbors.push_back(neighbor);
}

std::vector<Field*> Field::GetNeighbors() const
{
	return _neighbors;
}

int Field::GetNeighborCount() const
{
	return _neighbors.size();
}

void Field::SetState(int state)
{
	_state = state;
}

int Field::GetState() const
{
	return _state;
}

int Field::GetScreenXPos(void)
{
	return _xPos;
}

int Field::GetScreenYPos(void)
{
	return _yPos;
}

int Field::GetXPos(void)
{
	return _x;
}

int Field::GetYPos(void)
{
	return _y;
}

FieldType& Field::GetType()
{
	return _type;
}

bool Field::operator<(const Field& rhs) const
{
	return rhs.GetF() < GetF();
}

void Field::SortNeighbors()
{
	std::sort(_neighbors.begin(), _neighbors.end());
}

void Field::SetG(float g)
{
	_g = g;
}

void Field::SetH(float h)
{
	_h = h;
}

float Field::GetG(void)
{
	return _g;
}

float Field::GetH(void)
{
	return _h;
}

float Field::GetF(void) const
{
	return _g + _h;
}

void Field::SetCameFrom(Field* field)
{
	_cameFrom = field;
}

Field* Field::GetCameFrom(void) const
{
	return _cameFrom;
}

int Field::GetCost(void) const
{
	return _type.GetWeight();
}

float Field::GetPheromoneLevel(void) const
{
	return _pheromoneLevel;
}

void Field::EvaporatePheromones(float p)
{
	_pheromoneLevel = (1-p)*_pheromoneLevel;
}

void Field::SetExtraCost(int cost)
{
	_extraCost = cost;
}

int Field::GetExtraCost(void) const
{
	return _extraCost;
}

void Field::SetStart(Sprite* start)
{
	_sprStart = start;
}

void Field::SetFinish(Sprite* finish)
{
	_sprFinish = finish;
}

void Field::SetFire(Sprite* fire)
{
	_sprFire = fire;
	_type.SetWeight(-1);
}

void Field::ClearStart()
{
	_sprStart = NULL;
}

void Field::ClearFinish()
{
	_sprFinish = NULL;
}

bool Field::HasNutrition()
{
	if(_sprFinish) 
	{
		return true;
	}

	return false;
}

void Field::AddPheromones(float deltaTau) 
{
	_pheromoneLevel += deltaTau;
}

int Field::GetFieldHeight()
{
	return FIELD_HEIGHT;
}

int Field::GetFieldWidth()
{
	return FIELD_WIDTH;
}

CollisionOutline Field::GetOutline()
{
	return _outline;
}